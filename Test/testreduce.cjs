const reduce = require('../reduce.cjs');
const data = require('../data.cjs');

let sum = reduce(data, (acc, curr) => {
    return acc + curr;
}, 0)
console.log(sum);